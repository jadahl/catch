/*
 * Copyright © 2022 Jonas Ådahl
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common.h"

static void
process_crash (int signo)
{
  pid_t crashed_pid;
  pid_t gdb_pid;
  g_autofree char *cmdline = NULL;

  crashed_pid = getpid ();

  cmdline = get_cmdline_string (crashed_pid, NULL);
  g_printerr ("Caught crash (%s) on %d (%s)\n",
              signal_to_string (signo),
              crashed_pid,
              cmdline);

  gdb_pid = fork ();
  if (gdb_pid == -1)
    {
      g_warning ("fork() failed: %s", g_strerror (errno));
      return;
    }

  if (gdb_pid == 0)
    {
      char pid_arg[20];

      g_printerr ("Running gdb...\n");
      snprintf (pid_arg, sizeof (pid_arg), "%d", crashed_pid);

      g_printerr ("Running gdb...\n");
      execlp ("gdb", "gdb", "-p", pid_arg, "--batch",
              "-ex", "thread apply all backtrace full",
              NULL);
      g_warning ("Failed to spawn gdb: %s", g_strerror (errno));
    }

  g_printerr ("Waiting on gdb (%d)\n", gdb_pid);
  if (waitpid (gdb_pid, NULL, 0) == -1)
    g_warning ("waitpid() on %d failed: %s", gdb_pid, g_strerror (errno));
  g_printerr ("gdb done\n");

  raise (signo);
}

void libcatch_init () __attribute__((constructor));

void
libcatch_init ()
{
  const int signals[] =
    {
      SIGSEGV,
      SIGABRT,
    };
  int i;

  g_printerr ("WARNING: Crashed process return values will be discarded; "
              "don't use in tests\n");

  for (i = 0; i < G_N_ELEMENTS (signals); i++)
    {
      struct sigaction new_action =
        {
          .sa_flags = SA_RESETHAND,
          .sa_handler = process_crash,
        };

      sigemptyset (&new_action.sa_mask);

      if (sigaction (signals[i], &new_action, NULL) != 0)
        {
          g_warning ("sigaction() failed: %s", g_strerror (errno));
          continue;
        }
    }
}
