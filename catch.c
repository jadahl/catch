/*
 * Copyright © 2022-2023 Jonas Ådahl
 * Copyright © 2023 Marco Trevisan
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _GNU_SOURCE

#include <backtrace.h>
#include <gio/gio.h>
#include <glib.h>
#include <libunwind-ptrace.h>
#include <libunwind.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common.h"

typedef struct UPT_info SmartUptInfo;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (SmartUptInfo, _UPT_destroy);

G_DEFINE_AUTO_CLEANUP_FREE_FUNC (unw_addr_space_t, unw_destroy_addr_space, NULL)

static GTree * parse_process_map (pid_t    pid,
                                  GError **error);

static gboolean generate_backtrace (GTree   *process_map,
                                    pid_t    pid,
                                    GError **error);

static gboolean process_crash (pid_t        pid,
                               int          signal,
                               GHashTable  *tracked_pids,
                               GError     **error);

typedef enum _MapEntryFlags
{
  MAP_ENTRY_FLAG_NONE = 0,
  MAP_ENTRY_FLAG_SELF = 1 << 0,
} MapEntryFlags;

typedef enum _WatchFlags
{
  WATCH_FLAG_NONE = 0,
  WATCH_FLAG_ATEXIT = 1 << 0,
  WATCH_FLAG_SIGALRM = 1 << 1,
  WATCH_FLAG_SIGTERM = 1 << 2,
  WATCH_FLAG_SIGKILL = 1 << 3,
} WatchFlags;

static char *
get_known_pid_cmdline (pid_t        pid,
                       GHashTable  *tracked_pids,
                       GError     **error)
{
  g_autofree char *cmdline = NULL;

  g_set_str (&cmdline, g_hash_table_lookup (tracked_pids, GINT_TO_POINTER (pid)));
  if (cmdline)
    return g_steal_pointer (&cmdline);

  return get_cmdline_string (pid, error);
}

static pid_t
start_tracked_process (const char  *file,
                       char       **argv,
                       WatchFlags   watch_flags,
                       GError     **error)
{
  pid_t child;
  int wstatus;

  child = fork ();

  if (child == -1)
    {
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "Failed to fork: %s", g_strerror (errno));
      return -1;
    }
  else if (child == 0)
    {
      if (ptrace (PTRACE_TRACEME, 0, NULL, NULL) == -1)
        {
          g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                       "Failed to let parent to trace: %s", g_strerror (errno));
          return -1;
        }

      execvp (file, argv);
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "execvp() failed: %s", g_strerror (errno));
      return -1;
    }

  if (waitpid (child, &wstatus, 0) == -1)
    {
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "waitpid() failed: %s", g_strerror (errno));
      return -1;
    }

  if (ptrace (PTRACE_SETOPTIONS, child, NULL,
              (PTRACE_O_TRACEFORK |
               PTRACE_O_EXITKILL |
               PTRACE_O_TRACEVFORK |
               PTRACE_O_TRACECLONE |
               PTRACE_O_TRACEEXIT |
               PTRACE_O_TRACEEXEC)) == -1)
    {
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "ptrace(PTRACE_SETOPTIONS) failed: %s", g_strerror (errno));
      return -1;
    }

  if (ptrace (PTRACE_CONT, child, NULL, 0) == -1)
    {
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "ptrace(PTRACE_CONT) failed: %s", g_strerror (errno));
      return -1;
    }

  return child;
}

static gboolean
process_exit (pid_t        pid,
              int          exit_code,
              GHashTable  *tracked_pids,
              GError     **error)
{
  g_autofree char *cmdline = NULL;
  g_autoptr (GTree) process_map = NULL;

  cmdline = get_known_pid_cmdline (pid, tracked_pids, error);
  if (!cmdline)
    return FALSE;

  g_printerr ("Child %d (%s) exited with an error (%d)\n",
              pid, cmdline, exit_code);

  process_map = parse_process_map (pid, error);
  if (!process_map)
    return FALSE;

  return generate_backtrace (process_map, pid, error);
}

static gboolean
process_trap (pid_t        pid,
              int          wstatus,
              GHashTable  *tracked_pids,
              WatchFlags   flags,
              GError     **error)
{
  int ptrace_event;
  int exit_status;
  pid_t child;

  ptrace_event = (wstatus >> 16) & 0xffff;

  if (!ptrace_event)
    return FALSE;

  switch (ptrace_event)
    {
    case PTRACE_EVENT_FORK:
    case PTRACE_EVENT_VFORK:
    case PTRACE_EVENT_CLONE:
      if (ptrace (PTRACE_GETEVENTMSG, pid, NULL, &child) == -1)
        {
          g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                       "Failed to get new child pid: %s", g_strerror (errno));
          return FALSE;
        }

      g_hash_table_insert (tracked_pids, GINT_TO_POINTER (child),
                           get_cmdline_string (child, NULL));
      break;
    case PTRACE_EVENT_EXIT:
      if (!(flags & (WATCH_FLAG_ATEXIT|WATCH_FLAG_SIGKILL)))
        return TRUE;

      if (ptrace (PTRACE_GETEVENTMSG, pid, NULL, &exit_status) == -1)
        {
          g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                       "Failed to get pid %d exit status: %s",
                       pid, g_strerror (errno));
          return FALSE;
        }

      if (WIFSIGNALED (exit_status))
        {
          exit_status = WTERMSIG (exit_status);

          switch (exit_status)
            {
            case SIGSEGV:
            case SIGABRT:
              /* We would handle these anyways as part of watch_processes() */
              return TRUE;
            case SIGALRM:
              if ((flags & WATCH_FLAG_SIGALRM))
                return TRUE;
              break;
            case SIGTERM:
              if ((flags & WATCH_FLAG_SIGTERM))
                return TRUE;
              break;
            case SIGKILL:
              if (!(flags & WATCH_FLAG_SIGKILL))
                {
                  /* Special case, if sigkill isn't tracked but we track exit */
                  g_assert ((flags & WATCH_FLAG_ATEXIT) != 0);
                  return process_exit (pid, exit_status + 128, tracked_pids, error);
                }
              break;
            default:
            }

          return process_crash (pid, exit_status, tracked_pids, error);
        }

      if (!WIFEXITED (exit_status))
        break;

      exit_status = WEXITSTATUS (exit_status);
      if (exit_status != 0)
        return process_exit (pid, exit_status, tracked_pids, error);
      break;
    case PTRACE_EVENT_EXEC:
    default:
      break;
    }

  return TRUE;
}

typedef struct _MapEntry
{
  intptr_t low;
  intptr_t high;
  intptr_t offset;
  MapEntryFlags flags;
  char *file_path;
} MapEntry;

typedef struct _ProcessMap
{
  GArray *entries;
} ProcessMap;

static void
map_entry_free (MapEntry *entry)
{
  g_free (entry->file_path);
  g_free (entry);
}

static MapEntry *
map_entry_new (intptr_t       low,
               intptr_t       high,
               intptr_t       offset,
               MapEntryFlags  flags,
               char          *file_path)
{
  MapEntry *entry;

  entry = g_new0 (MapEntry, 1);
  entry->low = low;
  entry->high = high;
  entry->offset = offset;
  entry->flags = flags;
  entry->file_path = g_strdup (file_path);

  return entry;
}

static MapEntry *
parse_map_line (const char     *line,
                MapEntryFlags   flags,
                GError        **error)
{
  intptr_t low, high, offset;
  char rwxp[4];
  char *file_path;
  int n;

  file_path = alloca (strlen(line) + 1);
  n = sscanf (line, "%lx-%lx %4c %lx %*s %*s %s",
              &low, &high, rwxp, &offset, file_path);
  if (n == 4)
    return NULL;

  if (n != 5)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                   "Failed to parse maps line");
      return NULL;
    }

  if (rwxp[2] != 'x')
    return NULL;

  return map_entry_new (low, high, offset, flags, file_path);
}

static int
map_entry_sort (gconstpointer a,
                gconstpointer b,
                gpointer      user_data)
{
  const MapEntry *entry_a = a;
  const MapEntry *entry_b = b;

  if (entry_b->low > entry_a->low)
    return -1;
  else if (entry_b->low < entry_a->high)
    return 1;
  else
    return 0;
}

static int
map_entry_search (gconstpointer a,
                  gconstpointer b)
{
  const MapEntry *entry = a;
  intptr_t pointer = (intptr_t) b;

  if (pointer < entry->low)
    return -1;
  else if (pointer > entry->high)
    return 1;
  else
    return 0;
}


static unw_word_t
map_entry_translate_ip (MapEntry   *entry,
                        unw_word_t  ip)
{
  if (entry->flags & MAP_ENTRY_FLAG_SELF)
    return ip;
  else
    return (ip - entry->low) + entry->offset;
}

static GTree *
parse_process_map (pid_t    pid,
                   GError **error)
{
  g_autofree char *map_path = NULL;
  g_autoptr (GFile) map_file = NULL;
  g_autoptr (GFileInputStream) file_stream = NULL;
  g_autoptr (GDataInputStream) data_stream = NULL;
  g_autoptr (GTree) process_map = NULL;
  gboolean is_first_entry = TRUE;

  map_path = g_strdup_printf ("/proc/%d/maps", pid);
  map_file = g_file_new_for_path (map_path);
  file_stream = g_file_read (map_file, NULL, error);
  if (!file_stream)
    return NULL;

  data_stream = g_data_input_stream_new (G_INPUT_STREAM (file_stream));

  process_map = g_tree_new_full (map_entry_sort, NULL,
                                 (GDestroyNotify) map_entry_free, NULL);

  while (TRUE)
    {
      g_autoptr (GError) local_error = NULL;
      g_autofree char *line = NULL;
      MapEntryFlags flags;
      MapEntry *entry;

      line = g_data_input_stream_read_line (data_stream, NULL, NULL,
                                            &local_error);
      if (local_error)
        {
          g_propagate_error (error, g_steal_pointer (&local_error));
          return NULL;
        }
      if (!line)
        break;

      if (is_first_entry)
        flags = MAP_ENTRY_FLAG_SELF;
      else
        flags = MAP_ENTRY_FLAG_NONE;

      entry = parse_map_line (line, flags, &local_error);
      if (local_error)
        {
          g_propagate_error (error, g_steal_pointer (&local_error));
          return NULL;
        }

      if (!entry)
        continue;

      is_first_entry = FALSE;

      g_tree_insert (process_map, entry, NULL);
    }

  return g_steal_pointer (&process_map);
}

typedef struct
{
  MapEntry *entry;
  int frame_number;
  const char *fallback_function_name;
} BtCallbackData;

static int
bt_callback (void       *user_data,
             uintptr_t   pc,
             const char *filename,
             int         line_number,
             const char *function)
{
  BtCallbackData *callback_data = user_data;
  g_autofree char *symbol = NULL;

  if (function)
    symbol = g_strdup_printf ("%s()", function);
  else if (callback_data->fallback_function_name)
    symbol = g_strdup_printf ("%s()", callback_data->fallback_function_name);
  else
    symbol = g_strdup_printf ("%p", (gpointer) pc);

  if (filename)
    {
      g_print ("#%d %s%s at %s:%d\n", callback_data->frame_number,
               callback_data->frame_number < 10 ? " " : "",
               symbol, filename, line_number);
    }
  else
    {
      g_print ("#%d %s%s in %s\n", callback_data->frame_number,
               callback_data->frame_number < 10 ? " " : "",
               symbol,
               callback_data->entry->file_path);
    }

  return 0;
}

static gboolean
generate_backtrace (GTree   *process_map,
                    pid_t    pid,
                    GError **error)
{
  g_auto (unw_addr_space_t) address_space = NULL;
  g_autoptr (SmartUptInfo) upt_info = NULL;
  unw_cursor_t cursor;
  int ret;
  MapEntry *current_entry = NULL;
  struct backtrace_state *current_bt_state = NULL;
  int frame_number = 0;

  address_space = unw_create_addr_space (&_UPT_accessors, 0);
  upt_info = _UPT_create (pid);

  if (!upt_info || !address_space)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_NO_SPACE, "Out of memory");
      return FALSE;
    }

  ret = unw_init_remote (&cursor, address_space, upt_info);
  if (ret < 0)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                   "Failed to init unwind remote: %d", ret);
      return FALSE;
    }

  while (TRUE)
    {
      unw_word_t ip, local_ip;
      char function_name[512] = {};
      GTreeNode *entry_node;
      MapEntry *entry;
      struct backtrace_state *bt_state;
      BtCallbackData callback_data;

      unw_get_reg (&cursor, UNW_REG_IP, &ip);
      unw_get_proc_name (&cursor, function_name,
                         sizeof (function_name) - 1, NULL);

      ip--;

      entry_node = g_tree_search_node (process_map, map_entry_search,
                                       (gpointer) ip);
      if (!entry_node)
        {
          g_printerr ("Couldn't find process map entry for %p\n", (gpointer) ip);
          goto cont;
        }

      entry = g_tree_node_key (entry_node);

      if (entry == current_entry)
        {
          bt_state = current_bt_state;
        }
      else
        {
          bt_state = backtrace_create_state (entry->file_path, 1, NULL, NULL);

          current_entry = entry;
          current_bt_state = bt_state;
        }

      local_ip = map_entry_translate_ip (entry, ip);
      callback_data = (BtCallbackData) {
        .frame_number = frame_number,
        .entry = entry,
        .fallback_function_name = function_name,
      };
      backtrace_pcinfo (bt_state, local_ip, bt_callback, NULL, &callback_data);

      frame_number++;

cont:
      if (unw_step (&cursor) <= 0)
        break;
    }

  return TRUE;
}

static gboolean
process_crash (pid_t        pid,
               int          signal,
               GHashTable  *tracked_pids,
               GError     **error)
{
  g_autofree char *cmdline = NULL;
  g_autoptr (GTree) process_map = NULL;

  cmdline = get_known_pid_cmdline (pid, tracked_pids, error);
  if (!cmdline)
    return FALSE;

  g_print ("Received signal %d (%s) on process %d (%s)\n",
           signal, signal_to_string (signal),
           pid, cmdline);

  process_map = parse_process_map (pid, error);
  if (!process_map)
    return FALSE;

  return generate_backtrace (process_map, pid, error);
}

static int
watch_processes (GHashTable *tracked_pids,
                 WatchFlags  flags,
                 pid_t       main_child)
{
  int exit_status = EXIT_SUCCESS;

  while (TRUE)
    {
      g_autoptr (GError) error = NULL;
      pid_t pid;
      int wstatus;
      int signal;
      int signal_to_forward;
      gboolean handled;

      pid = waitpid (-1, &wstatus, 0);
      if (pid == -1)
        {
          g_printerr ("Failed to wait for status: %s\n", g_strerror (errno));
          return EXIT_FAILURE;
        }

      if (WIFSTOPPED (wstatus))
        signal = WSTOPSIG (wstatus);
      else
        signal = 0;

      switch (signal)
        {
        case SIGTRAP:
          handled = process_trap (pid, wstatus, tracked_pids, flags, &error);
          if (error)
            {
              g_printerr ("Failed to handle trap signal: %s\n", error->message);
              return EXIT_FAILURE;
            }
          if (handled)
            {
              signal_to_forward = 0;
              break;
            }

          G_GNUC_FALLTHROUGH;
        case SIGALRM:
          if (signal == SIGALRM && !(flags & WATCH_FLAG_SIGALRM))
            {
              signal_to_forward = signal;
              break;
            }
          G_GNUC_FALLTHROUGH;
        case SIGTERM:
          if (signal == SIGTERM && !(flags & WATCH_FLAG_SIGTERM))
            {
              signal_to_forward = signal;
              break;
            }
          G_GNUC_FALLTHROUGH;
        case SIGABRT:
        case SIGSEGV:
          if (!process_crash (pid, signal, tracked_pids, &error))
            {
              g_printerr ("Failed to handle crash (%d): %s\n", signal, error->message);
              return EXIT_FAILURE;
            }
          G_GNUC_FALLTHROUGH;
        default:
          if (WIFSTOPPED (wstatus))
            signal_to_forward = WSTOPSIG (wstatus);
          else if (WIFSIGNALED (wstatus))
            signal_to_forward = WTERMSIG (wstatus);
          else if (WIFEXITED (wstatus))
            signal_to_forward = WEXITSTATUS (wstatus);
          else
            signal_to_forward = signal;
          break;
        }

      if (WIFEXITED (wstatus) || WIFSIGNALED (wstatus))
        {
          int child_exit_status;
          int child_signal;
          g_autofree char *child_cmdline = NULL;

          g_hash_table_steal_extended (tracked_pids,
                                       GINT_TO_POINTER (pid),
                                       NULL, (gpointer *) &child_cmdline);

          if (WIFEXITED (wstatus))
            {
              child_signal = 0;
              child_exit_status = WEXITSTATUS (wstatus);
            }
          else if (WIFSIGNALED (wstatus))
            {
              child_signal = WTERMSIG (wstatus);
              child_exit_status = 128 + child_signal;
            }

          if (child_exit_status != EXIT_SUCCESS)
            {
              if (child_signal)
                {
                  g_printerr ("Child %d (%s) exited because of signal %d (%s)\n",
                              pid, child_cmdline, child_signal,
                              signal_to_string (child_signal));
                }
              else if (!(flags & WATCH_FLAG_ATEXIT))
                {
                  g_printerr ("Child %d (%s) exited with an error (%d)\n",
                              pid, child_cmdline, child_exit_status);
                }
            }

          if (pid == main_child)
            exit_status = child_exit_status;

          if (g_hash_table_size (tracked_pids) == 0)
            return exit_status;

          continue;
        }

      if (ptrace (PTRACE_CONT, pid, NULL, signal_to_forward) == -1)
        {
          g_printerr ("Failed to continue traced process: %s\n", g_strerror (errno));
          return EXIT_FAILURE;
        }
    }
}

static pid_t
ld_preload_launch (const char  *file,
                   char       **argv,
                   GError     **error)
{
  pid_t child;

  child = fork ();

  if (child == -1)
    {
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "Failed to fork: %s", g_strerror (errno));
      return -1;
    }
  else if (child == 0)
    {
      setenv ("LD_PRELOAD", "libcatch.so", 1);
      execvp (file, argv);
      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (errno),
                   "execvp() failed: %s", g_strerror (errno));
      return -1;
    }
  else
    {
      return child;
    }
}

static void
print_usage (char **argv)
{
  g_printerr ("Usage: %s [--help|-h | --sigalrm | --atexit | --sigterm | --sigkill] "
              "[--] <command> [<arg>]\n",
              argv[0]);
}

static void
print_help (void)
{
  g_printerr ("Catches SIGSEGV and SIGABRT of any subprocess that is spawned "
              "by <command>,\n"
              "as well as from <command> itself, and reports information about the "
              "state of\n"
              "the process at the signal was received. Requires ptrace() to "
              "work.\n");
  g_printerr ("\n");
  g_printerr ("Available options:\n");
  g_printerr ("  --help, -h             - Show this help text\n");
  g_printerr ("  --sigalrm              - Also catch SIGALRM\n");
  g_printerr ("  --sigterm              - Also catch SIGTERM\n");
  g_printerr ("  --sigkill              - Also catch SIGKILL\n");
  g_printerr ("  --atexit               - Also catch non-zero exit codes\n");
}

int
main (int    argc,
      char **argv)
{
  pid_t tracked_child;
  g_autoptr (GError) error = NULL;
  g_autoptr (GHashTable) tracked_pids = NULL;
  WatchFlags watch_flags = WATCH_FLAG_NONE;
  int consumed_args = 1;

  if (argc < 2)
    {
      print_usage (argv);
      return EXIT_FAILURE;
    }

  if (strcmp (argv[1], "--gdb") == 0)
    {
      pid_t pid;
      int wstatus;

      if (argc < 3)
        {
          print_usage (argv);
          return EXIT_FAILURE;
        }

      pid = ld_preload_launch (argv[2], argv + 2, &error);
      if (pid == -1)
        {
          g_warning ("Failed to start process: %s", error->message);
          return EXIT_FAILURE;
        }

      if (waitpid (pid, &wstatus, 0) == -1)
        {
          g_warning ("Failed to wait for %d: %s", pid, g_strerror (errno));
          return EXIT_FAILURE;
        }

      return WEXITSTATUS (wstatus);
    }

  while (consumed_args < argc && argv[consumed_args][0] == '-')
    {
      if (g_str_equal (argv[consumed_args], "--help") ||
          g_str_equal (argv[consumed_args], "-h"))
        {
          print_usage (argv);
          print_help ();
          return EXIT_SUCCESS;
        }
      else if (g_str_equal (argv[consumed_args], "--sigalrm"))
        {
          watch_flags |= WATCH_FLAG_SIGALRM;
        }
      else if (g_str_equal (argv[consumed_args], "--sigterm"))
        {
          watch_flags |= WATCH_FLAG_SIGTERM;
        }
      else if (g_str_equal (argv[consumed_args], "--sigkill"))
        {
          watch_flags |= WATCH_FLAG_SIGKILL;
        }
      else if (g_str_equal (argv[consumed_args], "--atexit"))
        {
          watch_flags |= WATCH_FLAG_ATEXIT;
        }
      else if (g_str_equal (argv[consumed_args], "--"))
        {
          consumed_args++;
          break;
        }
      else
        {
          print_usage (argv);
          return EXIT_FAILURE;
        }

      consumed_args++;
    }

  if (consumed_args == argc)
    {
      print_usage (argv);
      return EXIT_FAILURE;
    }

  tracked_child = start_tracked_process (argv[consumed_args],
                                         argv + consumed_args,
                                         watch_flags,
                                         &error);
  if (tracked_child == -1)
    {
      g_printerr ("Failed to start tracked process: %s\n", error->message);
      return EXIT_FAILURE;
    }

  tracked_pids = g_hash_table_new_full (NULL, NULL, NULL, g_free);
  g_hash_table_insert (tracked_pids, GINT_TO_POINTER (tracked_child),
                       get_cmdline_string (tracked_child, NULL));

  return watch_processes (tracked_pids, watch_flags, tracked_child);
}
