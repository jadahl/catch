project('catch', 'c',
  version: '0.1',
  meson_version: '>= 0.56.0',
  license: 'MIT',
  default_options: [
    'c_std=c99',
  ],
)

libbacktrace_subproject = subproject('libbacktrace')

glib_dep = dependency('glib-2.0', version: '>= 2.76')
gio_dep = dependency('gio-2.0')
libunwind_dep = dependency('libunwind')
libunwind_ptrace_dep = dependency('libunwind-ptrace')
libbacktrace_dep = dependency('backtrace',
  fallback: ['libbacktrace', 'libbacktrace_dep'],
)

bash = find_program('bash')

catch = executable('catch',
  sources: [
    'catch.c',
    'common.h',
  ],
  dependencies: [
    glib_dep,
    gio_dep,
    libunwind_dep,
    libunwind_ptrace_dep,
    libbacktrace_dep,
  ],
  install: true,
)

libcatch = shared_library('catch',
  sources: [
    'common.h',
    'libcatch.c',
  ],
  dependencies: [
    glib_dep,
    gio_dep,
  ],
  install: true,
)

test('catch true',
  catch,
  args: 'true',
)

test('catch false',
  catch,
  args: 'false',
  should_fail: true,
)

bash_tests = {
  'no args separator': {
    'args': ['true'],
  },

  'args separator': {
    'args': ['--', 'true'],
  },

  'invalid arg': {
    'args': ['--notexisting'],
    'expected_exit_code': 1,
    'script_checks': '''
      test_usage_is_shown
    ''',
  },

  'missing command': {
    'expected_exit_code': 1,
    'script_checks': '''
      test_usage_is_shown
    ''',
  },

  'missing command after arg': {
    'args': ['--sigabrt'],
    'expected_exit_code': 1,
    'script_checks': '''
      test_usage_is_shown
    ''',
  },

  'missing command separator': {
    'args': ['--'],
    'expected_exit_code': 1,
    'script_checks': '''
      test_usage_is_shown
    ''',
  },

  'binary not exists': {
    'args': ['this-binary does not exists!'],
    'expected_exit_code': 1,
    'script_checks': '''
      ! test -s $output
      grep -Fqs "Failed to start tracked process" $output
      grep -Fqs "No such file or directory" $output
    '''
  },
}

catch_bash_tests = {
  'help': {
    'args': ['--help'],
    'script': '',
    'script_checks': '''
      test -s $output
      grep -Fsq "Usage: @0@" $output
      grep -Fsq "Catches SIGSEGV and SIGABRT of any subprocess that is spawned" $output
      grep -Fsq "Available options" $output
    '''.format(catch.full_path())
  },

  'help short': {
    'args': ['-h'],
    'script': '',
    'script_checks': '''
      test -s $output
      grep -Fsq "Usage: @0@" $output
      grep -Fsq "Catches SIGSEGV and SIGABRT of any subprocess that is spawned" $output
      grep -Fsq "Available options" $output
    '''.format(catch.full_path())
  },

  'bash help': {
    'script': '@0@ --help'.format(bash.full_path()),
    'script_checks': '''
      test -s $output
      grep -Fsq "GNU bash" $output
    '''
  },

  'wrong option': {
    'args': ['-notexists'],
    'script': '',
    'expected_exit_code': 1,
    'script_checks': '''
      test -s $output
      grep -Fsq "" $output
    '''
  },

  'normal exit': {
    'script': 'exit 0',
    'script_checks': '''
      ! test -s $output
      test_not_exited_with_error
      test_signal_not_received 0
      test_not_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'normal exit failing child': {
    'script': 'false & wait -f %1; exit 0',
    'script_checks': '''
      ! test -s $output
      test_exited_with_error 1
      test_signal_not_received 0
      test_not_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'atexit normal exit': {
    'args': ['--atexit'],
    'script': 'exit 0',
    'script_checks': '''
      ! test -s $output
      test_not_exited_with_error
      test_signal_not_received 0
      test_not_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'atexit normal normal child exit': {
    'args': ['--atexit'],
    'script': 'true & wait -f %1; exit 0',
    'script_checks': '''
      ! test -s $output
      test_not_exited_with_error
      test_signal_not_received 0
      test_not_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'atexit failed child exit': {
    'args': ['--atexit'],
    'script': 'false & wait -f %1; exit 0',
    'script_checks': '''
      ! test -s $output
      test_exited_with_error 1
      test_signal_not_received 0
      test_signal_not_received 1
      test_not_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'failed exit': {
    'script': 'exit 135',
    'expected_exit_code': 135,
    'script_checks': '''
      test -s $output
      test_exited_with_error
      test_signal_not_received 135
      test_not_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'failed exit normal child': {
    'script': 'true & wait -f %1; exit 153',
    'expected_exit_code': 153,
    'script_checks': '''
      ! test -s $output
      test_exited_with_error 153
      test_signal_not_received 0
      test_not_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'atexit failed exit': {
    'args': ['--atexit'],
    'script': 'exit 155',
    'expected_exit_code': 155,
    'script_checks': '''
      test -s $output
      test_exited_with_error
      test_signal_not_received 155
      test_not_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit failed exit failed child': {
    'args': ['--atexit'],
    'script': 'false & wait -f %1; exit 153',
    'expected_exit_code': 153,
    'script_checks': '''
      test_exited_with_error 1
      test_exited_with_error 153
      test_signal_not_received 1
      test_signal_not_received 153
      test_not_exited_because_signal
      test_stacktrace_printed 2
    '''
  },

  'ignored SIGALRM': {
    'script': 'kill -SIGALRM $$',
    'expected_exit_signal': 14,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'tracked SIGALRM': {
    'args': ['--sigalrm'],
    'script': 'kill -SIGALRM $$',
    'expected_exit_signal': 14,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit SIGALRM': {
    'args': ['--atexit'],
    'script': 'kill -SIGALRM $$',
    'expected_exit_signal': 14,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit tracked SIGALRM': {
    'args': ['--atexit', '--sigalrm'],
    'script': 'kill -SIGALRM $$',
    'expected_exit_signal': 14,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'SIGSEGV': {
    'script': 'kill -SIGSEGV $$',
    'expected_exit_signal': 11,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'SIGSEGV child': {
    'script': 'sleep infinity & kill -SIGSEGV %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 11,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'SIGSEGV SIGABRT child': {
    'script': 'sleep infinity & kill -SIGABRT %1; kill -SIGSEGV $$',
    'expected_exit_signal': 11,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_signal_received 6
      test_exited_because_signal 6
      test_stacktrace_printed 2
      '''
  },

  'atexit SIGSEGV': {
    'args': ['--atexit'],
    'script': 'kill -SIGSEGV $$',
    'expected_exit_signal': 11,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'SIGABRT': {
    'script': 'kill -SIGABRT $$',
    'expected_exit_signal': 6,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'SIGABRT child': {
    'script': 'sleep infinity & kill -SIGABRT %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 6,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'SIGABRT SIGSEGV child': {
    'script': 'sleep infinity & kill -SIGSEGV %1; kill -SIGABRT $$',
    'expected_exit_signal': 6,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_signal_received 11
      test_exited_because_signal 11
      test_stacktrace_printed 2
      '''
  },

  'atexit SIGABRT': {
    'args': ['--atexit'],
    'script': 'kill -SIGABRT $$',
    'expected_exit_signal': 6,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'ignored SIGINT': {
    'script': 'kill -SIGINT $$',
    'expected_exit_signal': 2,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'ignored SIGTERM': {
    'script': 'kill -SIGTERM $$',
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'ignored SIGTERM child': {
    'script': 'sleep infinity & kill -SIGTERM %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'tracked SIGTERM': {
    'args': ['--sigterm'],
    'script': 'kill -SIGTERM $$',
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'tracked SIGTERM child': {
    'args': ['--sigterm'],
    'script': 'sleep infinity & kill -SIGTERM %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit SIGTERM': {
    'args': ['--atexit'],
    'script': 'kill -SIGTERM $$',
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit SIGTERM child': {
    'args': ['--atexit'],
    'script': 'sleep infinity & kill -SIGTERM %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit tracked SIGTERM': {
    'args': ['--atexit', '--sigterm'],
    'script': 'kill -SIGTERM $$',
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit tracked SIGTERM child': {
    'args': ['--sigterm', '--atexit'],
    'script': 'sleep infinity & kill -SIGTERM %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 15,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'ignored SIGKILL': {
    'script': 'kill -SIGKILL $$',
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'ignored SIGKILL child': {
    'script': 'sleep infinity & kill -SIGKILL %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_not_printed
    '''
  },

  'tracked SIGKILL': {
    'args': ['--sigkill'],
    'script': 'kill -SIGKILL $$',
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'tracked SIGKILL child': {
    'args': ['--sigkill'],
    'script': 'sleep infinity & kill -SIGKILL %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit SIGKILL': {
    'args': ['--atexit'],
    'script': 'kill -SIGKILL $$',
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit SIGKILL child': {
    'args': ['--atexit'],
    'script': 'sleep infinity & kill -SIGKILL %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_not_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit tracked SIGKILL': {
    'args': ['--atexit', '--sigkill'],
    'script': 'kill -SIGKILL $$',
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },

  'atexit tracked SIGKILL child': {
    'args': ['--atexit', '--sigkill'],
    'script': 'sleep infinity & kill -SIGKILL %1',
    'expected_exit_code': 0,
    'expected_exit_signal': 9,
    'script_checks': '''
      test_signal_received
      test_exited_because_signal
      test_stacktrace_printed
    '''
  },
}

foreach test_name, params: catch_bash_tests
  bash_tests += {
    test_name: params + {
      'args': params.get('args', []) + [
        '--',
        bash.full_path(),
        '-c',
        '\'@0@\''.format(params['script']),
      ]
    }
  }
endforeach

foreach test_name, params: bash_tests
  test(test_name,
    bash,
    args: [
      '-xc',
      '''
      output=$(mktemp -t 'catch-test-output-XXXXXX')
      trap 'rm $output' EXIT
      set -e
      @0@ @1@ 2>&1 | tee $output
      test ${PIPESTATUS[0]} = @2@
      function test_stacktrace_not_printed() {
        test "$(grep -c -Fs -n -- "#0 " $output)" -eq 0
      }
      function test_stacktrace_printed() {
        local num=${1:-1}
        test "$(grep -c -Fs -n -- "#0 " $output)" -eq $num
      }
      function test_exited_with_error() {
        local error=${1:-@2@}
        grep -Fqs -- "exited with an error ($error)" $output
      }
      function test_not_exited_with_error() {
        ! test_exited_with_error
        ! grep -Fqs -- "exited with an error" $output
      }
      function test_exited_because_signal() {
        local signal=${1:-@3@}
        local signal_name=${2:-SIG$(kill -l $signal)}
        grep -Fqs -- "exited because of signal $signal ($signal_name)" $output
      }
      function test_not_exited_because_signal() {
        ! test_exited_because_signal
        ! grep -Fqs -- "exited because of signal" $output
      }
      function test_signal_received() {
        local signal=${1:-@3@}
        local signal_name=${2:-SIG$(kill -l $signal)}
        grep -Fqs -- "Received signal $signal ($signal_name)" $output
      }
      function test_signal_not_received() {
        ! test_signal_received $@
      }
      function test_usage_is_shown() {
        test -s $output
        grep -Fsq "Usage: @0@" $output
        ! grep -Fsq "Available options" $output
      }
      @4@
      '''.format(
        params.get('command', catch.full_path()),
        ' '.join(params.get('args', [])),
        params.get('expected_exit_code',
          params.has_key('expected_exit_signal') ?
            params['expected_exit_signal'] + 128 : 0),
        params.get('expected_exit_signal', 0),
        params.get('script_checks', ''),
      ),
    ],
    env: [
      'LANG=C',
    ] + params.get('env', []),
    depends: [catch] + params.get('depends', []),
  )
endforeach

