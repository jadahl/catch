catch
=====

`catch` is a simple tool that uses `ptrace()` to catch crashes (`SIGSEGV`,
`SIGABRT`) of spawned child processes. It uses libunwind and libunwind-ptrace
to unwind the backtrace, and libbacktrace to extract debug information, such as
function name and source code file and line number.

It watches for crashes on all spawned child processes, including processes
spawned by children, meaning it can for example be run to gather backtraces
from a test suite.

Under normal circumstances, an utility like this is not necessary, and debug
information about crashes should be caught by e.g. `coredumpctl`, but sometimes
that's not possible, for example in continuous integration pipelines.

Dependencies
============

`catch` depends on libunwind, libunwind-ptrace, glib, and a custom libbacktrace
that adds meson build system integration. libbacktrace is built as a static
library via a meson subproject.

License
=======

`catch` is released under the MIT license.
